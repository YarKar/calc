﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calc.Service
{
    public class CalcApp
    {
        public Func<int, int , double> _add = (a,b) => a + b;

        public Func<int, int, double> _sub = (a, b) => a - b;

        private Func<int, int, double> _mult = (a, b) => a * b;

        private Func<int, int, double> _div = (a, b) => b != 0 ? a / b : double.NaN;

        public double Calculate(int a, int b, Func<int, int, double> operation)
        {
            return operation(a, b);    
        }
    }
}
