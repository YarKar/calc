﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calc.Gui
{
    public partial class MainForm : Form
    {
        private double _r0;
        private double _r1;
        private Func<double, double, double> operation;

        public MainForm()
        {
            _r0 = default(double);
            _r1 = default(double);
            InitializeComponent();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void AddValue(double value)
        {
            _r0 = _r0 * 10 + value;

            this.lblR0.Text = _r0.ToString();
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            AddValue(0);
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            AddValue(1);
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            AddValue(2);
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            AddValue(3);
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            AddValue(4);
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            AddValue(5);
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            AddValue(6);
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            AddValue(7);
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            AddValue(8);
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            AddValue(9);
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SaveValues();
            operation = (a, b) => a + b;
        }

        private void SaveValues()
        {
            if (_r1 == default(double)) _r1 = _r0;
            _r0 = default(double);
        }

        private void btnSub_Click(object sender, EventArgs e)
        {
            SaveValues();

            operation = (a, b) => a - b;
        }

        private void btnMult_Click(object sender, EventArgs e)
        {
            SaveValues();

            operation = (a, b) => a * b;
        }

        private void btnDiv_Click(object sender, EventArgs e)
        {
            SaveValues();

            operation = (a, b) => b != 0 ? a / b : double.NaN;
        }

        private void btnCalc_Click(object sender, EventArgs e)
        {
            _r1 = operation(_r1, _r0);

            lblR0.Text = _r1.ToString();
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            _r0 = default(double);
            _r1 = default(double);

            lblR0.Text = _r0.ToString();
        }
    }                         
}
